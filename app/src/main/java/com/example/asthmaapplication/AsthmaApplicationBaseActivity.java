package com.example.asthmaapplication;

public class AsthmaApplicationBaseActivity {

    public enum NavigationOption {
        AUTHENTICATION,
        UNSELECTED
    }

    NavigationOption navigationOption = NavigationOption.UNSELECTED;

    public void setNavigationOption(NavigationOption navigationOption) {
        this.navigationOption = navigationOption;
    }
}
