package UserAuthentication;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import extensions.FragmentScoped;

@Module
public abstract class UserAuthenticationModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract UserLoginFragment userLoginFragment();

}
