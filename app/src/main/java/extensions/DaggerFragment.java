package extensions;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.annotation.ContentView;
import androidx.annotation.LayoutRes;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.internal.Beta;
import javax.inject.Inject;

@Beta
public abstract class DaggerFragment extends Fragment implements  HasAndroidInjector{

    @Inject DispatchingAndroidInjector<Object> androidInjector;
    /**
     * A {@link Fragment} that injects its members in {@link #onAttach(Context)} and can be used to inject child Fragments
     * attached to it. Note that when this fragment gets reattached, its members will be injected again.
     */

    public DaggerFragment() { super(); }

    @ContentView
    public DaggerFragment(@LayoutRes int contentLayoutId) { super(contentLayoutId); }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public AndroidInjector<Object> androidInjector() { return androidInjector; }

}
